package com.sobsoft.exception;
import org.apache.log4j.Logger;

public class Log {
    private static final Logger log = Logger.getLogger(Log.class);

    public void unknownFigure() {
        System.out.println("We've tried to draw figure.");
        log.error("Figure type is unknown.");
    }
}
