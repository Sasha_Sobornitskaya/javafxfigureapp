package com.sobsoft.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Trapezium extends Figure{
    private double halfUpBase;
    private double halfDownBase;
    private double halfHight;

    public Trapezium(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_TRAPEZIUM, cx, cy, lineWidth, color);
    }

    public Trapezium(double cx, double cy, double lineWidth, Color color, double halfUpBase, double halfDownBase, double halfHight) {
        this(cx, cy, lineWidth, color);
        this.halfUpBase = halfUpBase < 10 ? 10 : halfUpBase;
        this.halfDownBase = halfDownBase < 10 ? 10 : halfDownBase;
        this.halfHight = halfHight < 10 ? 10 : halfHight;
    }

    public double getUpBase() {
        return halfUpBase;
    }

    public double getDownBase() {
        return halfDownBase;
    }

    public double getHalfHight() {
        return halfHight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trapezium trapezium = (Trapezium) o;
        return Double.compare(trapezium.halfUpBase, halfUpBase) == 0 &&
                Double.compare(trapezium.halfDownBase, halfDownBase) == 0 &&
                Double.compare(trapezium.halfHight, halfHight) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(halfUpBase, halfDownBase, halfHight);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "halfHeight=" + halfUpBase +
                ", halfWidth=" + halfDownBase +
                '}';
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokePolygon(new double[]{cx - halfDownBase, cx - halfUpBase, cx + halfUpBase, cx + halfDownBase},
                new double[]{cy - halfHight, cy + halfHight, cy + halfHight, cy - halfHight}, 4);
    }
}