package com.sobsoft.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Hexagon extends Figure {
    private double base;

    public Hexagon(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_HEXAGON, cx, cy, lineWidth, color);
    }

    public Hexagon(double cx, double cy, double lineWidth, Color color, double Base) {
        this(cx, cy, lineWidth, color);
        this.base = Base < 10 ? 10 : Base;
    }

    public double getBase() {
        return base;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hexagon hexagon = (Hexagon) o;
        return Double.compare(hexagon.base, base) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(base);
    }

    @Override
    public String toString() {
        return "com.sobsoft.figuresfx.figures.Triangle{" +
                ", halfBase=" + base +
                '}';
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokePolygon(new double[]{cx - base, cx - base / 2, cx + base / 2, cx + base, cx + base / 2, cx - base / 2},
                new double[]{cy, cy + base * Math.sqrt(3) / 2, cy + base * Math.sqrt(3) / 2, cy, cy - base * Math.sqrt(3) / 2, cy - base * Math.sqrt(3) / 2}, 6);
    }
}
