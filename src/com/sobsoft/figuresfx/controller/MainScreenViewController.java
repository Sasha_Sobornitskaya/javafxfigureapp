package com.sobsoft.figuresfx.controller;

import com.sobsoft.drawUtils.Drawer;
import com.sobsoft.exception.Log;
import com.sobsoft.exception.UnknownFigureTypeExсeption;
import com.sobsoft.figuresfx.figures.*;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

import com.sobsoft.figuresfx.figures.Circle;
import com.sobsoft.figuresfx.figures.Figure;
import com.sobsoft.figuresfx.figures.Rectangle;
import com.sobsoft.figuresfx.figures.Triangle;
import org.apache.log4j.Logger;

public class MainScreenViewController implements Initializable {
    public Button screenCleaner;
    private ArrayList<Figure> figures = new ArrayList<>();
    private Random random;
    Drawer drawer = new Drawer(figures);
    private static final Logger log = Logger.getLogger(Log.class);

    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        random = new Random(System.currentTimeMillis());
    }

    private void addFigure(Figure figure){
        figures.add(figure);
    }

    private Figure createFigure(double x, double y) throws UnknownFigureTypeExсeption {
        Figure figure = null;

        switch (random.nextInt(5)){
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(x, y, random.nextInt(3), Color.RED, random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(x, y, random.nextInt(3), Color.BLUE, random.nextInt(50), random.nextInt(100));
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, random.nextInt(3), Color.GREEN, (double)random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_HEXAGON:
                figure = new Hexagon(x, y, random.nextInt(3), Color.BLUEVIOLET, (double)random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_TRAPEZIUM:
                figure = new Trapezium(x, y, random.nextInt(3), Color.DEEPPINK, random.nextInt(100), random.nextInt(100), random.nextInt(50));
                break;
            default:try {
                throw new UnknownFigureTypeExсeption("Unknown figure");
            } catch (Exception e) {
                log.error("Something failed", e);
            }

        }
        return figure;
    }

    private void repaint(){
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for (Figure figure: figures) {
            if(figure != null){
                drawer.draw(canvas.getGraphicsContext2D());
            }
        }
    }

    @FXML
    private void screenClean(){
        screenCleaner.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
        public void handle(MouseEvent mouseEvent) {
            figures.clear();
            canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        }
    });

    }

    @FXML
    private void onMouseClicked(MouseEvent mouseEvent) throws UnknownFigureTypeExсeption {
        addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        repaint();
    }
}
